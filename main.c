//
// Created by natha on 11/04/2024.
//

// Getraenke Automat Version 0.1

#include <stdio.h>
int main() {

    int auswahl = 0;
    int num_items = 0;
    int i;

    float einwurf = 0;
    float remainder = 0;
    float price_water = 0.5;
    float price_limo = 1.0;
    float price_beer = 2.0;
    float price = 0.0;

    printf("Getraenke Automat v0.2\n"
           "\n"
           "Waehlen sie ihr Getraenk aus:\n"
           "1) Wasser (%.2f Euro)\n"
           "2) Limonade (%.2f Euro)\n"
           "3) Bier (%.2f Euro)\n"
           "Geben Sie Wahl und Menge von 1, 2 oder 3 ein:\n", price_water, price_limo, price_beer);

    scanf("%d", &auswahl);
    if (scanf("%d", &num_items) != 1) return 1;

    switch(auswahl){
        case 1:
            price = price_water * num_items;
            printf("Sie haben %d mal Wasser ausgewählt.\n"
                   "Bitte %.2f Euro einwerfen.\n",num_items, price);

            break;
        case 2:
            price = price_limo * num_items;
            printf("Sie haben %d mal Limonade ausgewählt.\n"
                   "Bitte %.2f Euro einwerfen.\n",num_items, price);
            break;
        case 3:
            price = price_beer * num_items;
            printf("Sie haben %d mal Bier ausgewählt.\n"
                   "Bitte %.2f Euro einwerfen.\n",num_items, price);

            break;
        default:
            printf("Ungültige Eingabe.");
            return 1;
    }
    printf("\n--- Bezahlvorgang ---\n");
    printf("Gesamtpreis: %.2f\n",price);
    remainder = price;

    do{
        printf("Es fehlen noch %.2f Euro",remainder);
        scanf("%f", &einwurf);
        remainder -= einwurf;
    }
    while(remainder > 0);

    printf("--- Getraenkeausgabe ---\n");

    for(i = 1; i <= num_items;i++){
        printf("Getränk %d von %d wird ausgegeben.\n", i, num_items);
    }

    if(remainder < 0) {printf("%.2f Euro Rückgeld.\n", remainder * -1);}
}